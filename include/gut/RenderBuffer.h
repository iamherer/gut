#pragma once

#include "gut/pch.h"


namespace Gut
{
    struct RenderBuffer
    {

        unsigned int ID;

        void Create();
        void Bind();
        ~RenderBuffer();

        void Storage(unsigned int internalFormat, int width, int height);
    };
}