#pragma once
#include <iostream>
#include <random>

namespace Gut
{

	struct UID
	{
		int64_t m_UUID;
		UID();
		void Generate();
		
		bool operator<(const UID &other) const
		{
			return m_UUID < other.m_UUID;
		}
	};
}

