#pragma once

#include "pch.h"

namespace Gut
{

	struct BlendDesc
	{
		bool enableBlend = false;
		int sfactor = GL_SRC_ALPHA;
		int dfactor = GL_ONE_MINUS_SRC_ALPHA;
		void Clear() const;
	};

	struct RasterizerDesc
	{
		bool enableCullFace = true;
		int cullFace = GL_BACK;
		int frontFace = GL_CCW;

		void Clear() const;
	};
	struct DepthStencilDesc
	{
		bool enableDepthTest = true;
		int depthFunc = GL_LEQUAL;

		bool enableStencilTest = false;
		int stencilFunc = GL_ALWAYS;
		int stencilRef = 0;
		int stencilRefMask = 0x00;

		int stencilFail = GL_KEEP;
		int stencilZFail = GL_KEEP;
		int stencilZPass = GL_REPLACE;

		int stencilMask = 0xFF;

		void Clear() const;
	};
};