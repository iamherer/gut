#pragma once

#include "gut/pch.h"

namespace Gut
{
	struct Viewport
	{
		float left;
		float right;
		float bottom;
		float top;
		float zNear;
		float zFar;
		glm::mat4 GetOrthoMatrix() const;
	};


}
