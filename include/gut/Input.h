#pragma once

#include <vector>
#include <map>
#include <iostream>
#include <GLFW/glfw3.h>

enum KeyMode
{
	_null = 0,
	_shift = 1,
	_ctrl = 2,
	_alt = 4,
	_win = 8,
};
