#pragma once

#include "gut/Vertex.h"
#include "gut/pch.h"

namespace Gut
{
    struct VertexPos
    {
        glm::vec3 pos;
        VertexPos() = default;
        VertexPos(const glm::vec3 &p) : pos(p)
        {
        }
        static const std::vector<Gut::VertexLayout> inputLayout;
    };

    struct VertexPosNormal
    {
        glm::vec3 pos;
        glm::vec3 normal;

        VertexPosNormal() = default;
        static const std::vector<Gut::VertexLayout> inputLayout;
    };

    struct VertexPosTex
    {
        glm::vec3 pos;
        glm::vec3 normal;
        glm::vec2 tex;

        VertexPosTex() = default;
        static const std::vector<Gut::VertexLayout> inputLayout;
    };

    struct VertexPosNormalTex
    {
        glm::vec3 pos;
        glm::vec3 normal;
        glm::vec2 tex;

        VertexPosNormalTex() = default;
        static const std::vector<Gut::VertexLayout> inputLayout;
    };
}