#pragma once

#include "gut/pch.h"

namespace Gut
{

	struct UniformBuffer
	{
		struct Desc
		{
			unsigned int dataSize;
			unsigned int usage = GL_DYNAMIC_DRAW;
			unsigned int binding;
			void *data = 0;
			unsigned int offset = 0;
		};

		unsigned int UBO;
		bool CreateFromDesc(const Gut::UniformBuffer::Desc &desc);

		template <typename T>
		bool CreateFrom(unsigned int binding)
		{
			UniformBuffer::Desc desc;
			desc.binding = binding;
			desc.dataSize = sizeof(T);

			return CreateFromDesc(desc);
		}

		void Upload(const void *data, size_t off, size_t len) const;
		void Upload(const void *data, size_t len) const;

		template <typename T>
		void Upload(const T &t) const
		{
			Upload((void *)&t, sizeof(T));
		}
		void Delete();
		virtual ~UniformBuffer()
		{
			Delete();
		}
	};
}