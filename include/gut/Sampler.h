#pragma once

#include "pch.h"

namespace Gut
{
	struct SamplerDesc
	{
		unsigned int baseLevel;
		unsigned int compareFunc;
		unsigned int compareMode;
		unsigned int minFilter;
		unsigned int magFilter;
		unsigned int minLod;
		unsigned int maxLod;
		unsigned int maxLevel;
		unsigned int swizzleR;
		unsigned int swizzleG;
		unsigned int swizzleB;
		unsigned int swizzleA;
		unsigned int wrapS;
		unsigned int wrapT;
		unsigned int wrapR;

		bool useBorderColor;
		float borderColor[4];

		SamplerDesc();
		void BindTexture(int target) const;
	};

	struct Sampler
	{
		unsigned int ID;

		bool CreateFromDesc(const SamplerDesc &desc)
		{
			glGenSamplers(1, &ID);
			glSamplerParameteri(ID, GL_TEXTURE_MIN_FILTER, desc.minFilter);
			glSamplerParameteri(ID, GL_TEXTURE_MAG_FILTER, desc.magFilter);
			glSamplerParameteri(ID, GL_TEXTURE_WRAP_S, desc.wrapS);
			glSamplerParameteri(ID, GL_TEXTURE_WRAP_R, desc.wrapR);
			glSamplerParameteri(ID, GL_TEXTURE_WRAP_T, desc.wrapT);

			return true;
		}
		void Bind(unsigned int tex)
		{
			glBindSampler(tex, this->ID);
		}
	};
}