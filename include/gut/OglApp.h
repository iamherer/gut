#pragma once

#include "gut/pch.h"
#include "gut/GLFWTimer.h"
#include "gut/InputSystem.h"

struct OglApp
{
    GLFWwindow *window = nullptr;

    struct Desc
    {
        int width = 800;
        int height = 600;
        int posX = 200;
        int posY = 200;
        bool fullScreen = false;
        char title[128] = "Main Window";
        bool mFullScreen;
        char imgui_ini_path[64] = "imgui.ini";
    };
    GLFWTimer m_Timer;
    Desc m_Desc;
    InputSystem inputSystem;

    OglApp(const Desc &desc);

    const float GetAspect();
    void SetFullScreen();
    void SetWindowMode();
    void CloseWindow();
    bool InitOpengl();
    void SetClipboard(const char* text);
    virtual bool Init();
    virtual void Execute();
    virtual void OnTick();
    virtual void OnAfterUpdate();
    virtual void OnDestroy();
    virtual void OnFrameRender();
    virtual void OnClose();

public:
    // callback
    virtual void OnFrameBufferSize(int w, int h);
    virtual void OnFocus(int e);
    virtual void OnMonitor(int e);

    // Callback
    virtual void OnCursorPos(double x, double y);
    virtual void OnMouse(int key, int action, int mode);
    virtual void OnKeyCallback(int, int, int, int);
    virtual void OnJoystick(int joy, int event);
    virtual void OnScroll(double x, double y);
    virtual void OnDrop(int count, const char **paths);

    virtual void OnWindowIconify(int ev);
    virtual void OnRefresh();
    virtual void OnWindowPos(int x, int y);
};