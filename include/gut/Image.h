#pragma once

#include "gut/pch.h"

#include "stb/stb_image.h"
#include "stb/stb_image_write.h"
#include "stb/stb_image_resize.h"

namespace Gut
{
    struct Image
    {
        void* data = 0;
        int width, height, nrComponent;

        uint8_t* Load(const char* path, int req = 4);
        void WriteJpg(const char* path, int quality);
        void WritePng(const char* path, int stride_bytes);
        void WirteBmp(const char* path);
        void Free();
    };
}
