#pragma once

#include "gut/pch.h"

namespace Gut
{

	struct Mesh
	{
		unsigned int VAO;

		void Delete()
		{
			glDeleteVertexArrays(1, &VAO);
		}
		virtual ~Mesh()
		{
			Delete();
		}
	};
}