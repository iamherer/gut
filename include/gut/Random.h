#pragma once

#include <iostream>
#include <ctime>
#include <random>
#include <glm/glm.hpp>

namespace Gut
{
	struct Random
	{
		std::default_random_engine e;

		void GetInt(int *nos, int num, int min, int max);
		
		void GetFloat(float *fs, float num, float min, float max);

		void GetVec2(glm::vec2 *target, int num, const glm::vec2 &min, const glm::vec2 &max);

		void GetVec3(glm::vec3 *target, int num, const glm::vec3 &min, const glm::vec3 &max);

		void GetVec4(glm::vec4* target, int num, const glm::vec4 &min, const glm::vec4 &max);

	};
};
