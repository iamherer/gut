#pragma once
#include "gut/pch.h"

#include "gut/Transform.h"
#include "gut/Frustum.h"

namespace Gut{

	struct Camera : public Transform, public Frustum
	{
		void SetAspect(const float &aspect);
	};
}
