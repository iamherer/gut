#pragma once

#include "gut/pch.h"
#include "gut/Texture.h"

namespace Gut
{

	struct FrameBuffer
	{
		unsigned int FBO;

		void BufferTextureLayer(unsigned int attachment, unsigned int texId, int level, int layer);

		void BufferTexture2D(unsigned int attachment, unsigned int texTarget, unsigned int ID, unsigned int mipLevel = 0);

		void BufferTextureLayer(unsigned int attachment, unsigned int tex, unsigned int mipLevel, unsigned int zOff);

		void FrameRenderBuffer(unsigned int attachment, unsigned int RBO);

		void ReadBuffer(unsigned int attachment);

		void DrawBuffers(unsigned int* attachments, int num);

		void Create();

		void CheckError();

		void Bind();

		void BufferTexture(unsigned int attachment, unsigned int texId, int level);

		void BufferTexture3D(unsigned int attachment, unsigned int texTarget, unsigned int tex, unsigned int mipLevel, unsigned int zOff);

		~FrameBuffer();

		static void UnBind();
	};

};