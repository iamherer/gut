#pragma once

#include "gut/pch.h"

namespace Gut {
	
	struct Frustum
	{
		float fovy = 45.0f;
		float aspect = 1.0f;
		float nNear = 0.1f, nFar = 10000.0f;
		glm::mat4 GetProjection() const;
		glm::mat4 GetProjection(const float& aspect) const;
	};
}
