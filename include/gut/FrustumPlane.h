#pragma once

#include "gut/Plane.h"
#include "gut/Transform.h"
#include "gut/Viewport.h"
#include "gut/Frustum.h"

namespace Gut
{

    struct FrustumPlane
    {
        Plane topFace;
        Plane bottomFace;

        Plane rightFace;
        Plane leftFace;

        Plane farFace;
        Plane nearFace;
    
        bool Create(const Gut::Transform& transform, const Gut::Frustum& frustum);
        bool Create(const Gut::Transform& transform, const Gut::Viewport& vp);
    };
};