#pragma once

#include "gut/Transform.h"
#include "gut/Viewport.h"

namespace Gut
{

	struct OrthoCamera : public Transform, public Viewport
	{

	};
}
