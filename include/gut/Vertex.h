#pragma once

#include "gut/pch.h"

namespace Gut
{
	struct VertexLayout
	{
		const char *name;
		uint32_t index;
		uint32_t size;
		uint32_t type;
		bool normalized;
		uint32_t offset;
	};

}