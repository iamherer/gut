#pragma once

#include "gut/RenderDesc.h"

namespace Gut
{
    struct RenderStatus
    {

        Gut::DepthStencilDesc dessDefault;
        Gut::BlendDesc bsDefault;
        Gut::RasterizerDesc rsDefault;

        bool hasInit = false;
        void Init();
        static RenderStatus *g_inst;
        static RenderStatus* Inst();

    };
}