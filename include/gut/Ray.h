#pragma once

#include "gut/pch.h"

namespace Gut
{
	struct Ray
	{
		glm::vec3 origin =glm::vec3(0.0f);
		glm::vec3 direction = glm::vec3(1.0f);

	};
};
