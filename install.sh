#!/bin/bash

create_if_not_exist()
{

if [ -d $1 ]; then 
    echo "has create the file"
else
    mkdir $1
fi

}

buildfolder="build"
create_if_not_exist $buildfolder 

rm -rf /usr/local/include/gut
cd $buildfolder 
cmake ..
make install
cd ..
