#include "gut/Image.h"


using namespace Gut;

uint8_t* Image::Load(const char* path, int req)
{

    this->data = stbi_load(path, &width, &height, &nrComponent, req);
    if (data == 0)
    {
       GUT_LOGI("cannot load image path %s", path);
    }

    return (uint8_t*)this->data;
}

void Image::WriteJpg(const char* path, int quality)
{
    stbi_write_jpg(path, width, height, this->nrComponent, this->data, quality);
}

void Image::WritePng(const char* path, int stride_bytes)
{
    stbi_write_png(path, width, height, this->nrComponent, this->data, stride_bytes);
}

void Image::WirteBmp(const char* path)
{
    stbi_write_bmp(path, width, height, this->nrComponent, this->data);
}

void Image::Free()
{
    // STBI_FREE(data);
    stbi_image_free(data);
}
