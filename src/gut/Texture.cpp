#include "gut/Texture.h"
#include "gut/Image.h"

bool Gut::Texture2D::CreateSTB(const std::string& path)
{
    this->m_Path = path;

	Image image;
    image.Load(path.c_str());

    Create();

    Gut::Texture2DDesc desc;
    desc.data = image.data;
    desc.width = image.width;
    desc.height = image.height;
    desc.format = GL_RGBA;
    desc.internalformat = GL_RGBA;
    desc.target = GL_TEXTURE_2D;
    desc.dataType = GL_UNSIGNED_BYTE;

    TexImage2D(desc);

    Gut::SamplerDesc sDesc;

    sDesc.wrapR = GL_CLAMP_TO_EDGE;
    sDesc.wrapT = GL_CLAMP_TO_EDGE;
    sDesc.magFilter = GL_LINEAR;
    sDesc.minFilter = GL_LINEAR;

    SetSampler(sDesc);
    glBindTexture(GL_TEXTURE_2D, 0);

    image.Free();

    return true;
}

// bool Gut::Texture2D::CreateHDR(const std::string& path)
// {
// 	Create();
// 	Bind();

// 	stbi_set_flip_vertically_on_load(true);
	
// 	int nrComponents;
// 	Texture2DDesc desc;
// 	desc.data = stbi_loadf(path.c_str(), &desc.width, &desc.height, &nrComponents, 0);

// 	if (desc.data == 0)
// 	{
// 		printf("Failed to load HDR image.");
// 		return false;
// 	}

// 	desc.dataType = GL_FLOAT;
// 	desc.internalformat = GL_RGB16F;
// 	desc.format = GL_RGB;
// 	desc.level = 0;
// 	desc.target = GL_TEXTURE_2D;
// 	TexImage2D(desc);

// 	stbi_image_free(desc.data);

// 	SamplerDesc sampDesc;
// 	sampDesc.wrapS = GL_CLAMP_TO_EDGE;
// 	sampDesc.wrapT = GL_CLAMP_TO_EDGE;
// 	sampDesc.minFilter = GL_LINEAR;
// 	sampDesc.magFilter = GL_LINEAR;

// 	SetSampler(sampDesc);
// 	stbi_set_flip_vertically_on_load(false);
// 	return true;
// }
