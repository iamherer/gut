#include "gut/UniformBuffer.h"

void Gut::UniformBuffer::Upload(const void *data, size_t off, size_t len) const
{
    glBindBuffer(GL_UNIFORM_BUFFER, UBO);
    glBufferSubData(GL_UNIFORM_BUFFER, off, len, data);
    glBindBuffer(GL_UNIFORM_BUFFER, 0);
}

void Gut::UniformBuffer::Upload(const void *data, size_t len) const
{
    Upload(data, 0, len);
}

bool Gut::UniformBuffer::CreateFromDesc(const UniformBuffer::Desc &desc)
{
    glGenBuffers(1, &UBO);
    glBindBuffer(GL_UNIFORM_BUFFER, UBO);
    glBufferData(GL_UNIFORM_BUFFER, desc.dataSize, desc.data, desc.usage);
    glBindBufferBase(GL_UNIFORM_BUFFER, desc.binding, UBO);

    return true;
};


void Gut::UniformBuffer::Delete()
{
    glDeleteBuffers(1, &UBO);
}