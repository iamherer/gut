#include "gut/MeshVertexArray.h"

using namespace Gut;

void MeshVertexArray::DrawArrays() const
{
    glBindVertexArray(this->VAO);
    glDrawArrays(this->faceType, 0, vertexNum);
    glBindVertexArray(0);
}

bool MeshVertexArray::CreateFromDesc(const MeshVertexArrayDesc &desc)
{
    bool gr = true;

    unsigned int VBO;
    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);

    glBindVertexArray(VAO);

    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, desc.vertexStride * desc.vertexNum, desc.vertexheader, desc.usage);

    for (int i = 0; i < desc.numLayout; i++)
    {
        const auto &l = desc.layout[i];

        glEnableVertexAttribArray(l.index);

        if (l.type == GL_FLOAT)
        {
            glVertexAttribPointer(l.index, l.size, l.type, l.normalized,
                                  desc.vertexStride, (void *)l.offset);
        }
        else if (l.type == GL_INT)
        {
            glVertexAttribIPointer(l.index, l.size, l.type, desc.vertexStride, (void *)l.offset);
        }
    }

    glBindVertexArray(0);

    this->vertexNum = desc.vertexNum;

    return true;
}