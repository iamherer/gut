#include "gut/Viewport.h"

using namespace Gut;

glm::mat4 Viewport::GetOrthoMatrix() const
{
	return glm::orthoRH(left, right, bottom, top, zNear, zFar);
}
