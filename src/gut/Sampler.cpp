#include "gut/Sampler.h"

using namespace Gut;

SamplerDesc::SamplerDesc()
{
	memset((char *)this, 0, sizeof(*this));
}

void SamplerDesc::BindTexture(int target) const
{

#ifdef WIN32
	if (useBorderColor)
	{
		glTexParameterfv(target, GL_TEXTURE_BORDER_COLOR, borderColor);
	}
#endif // DEBUG

	if (wrapS)
		glTexParameteri(target, GL_TEXTURE_WRAP_S, wrapS);
	if (wrapT)
		glTexParameteri(target, GL_TEXTURE_WRAP_T, wrapT);
	if (wrapR)
		glTexParameteri(target, GL_TEXTURE_WRAP_R, wrapR);
	if (minFilter)
		glTexParameteri(target, GL_TEXTURE_MIN_FILTER, minFilter);
	if (magFilter)
		glTexParameteri(target, GL_TEXTURE_MAG_FILTER, magFilter);
	if (minLod)
		glTexParameteri(target, GL_TEXTURE_MIN_LOD, minLod);
	if (maxLod)
		glTexParameteri(target, GL_TEXTURE_MAX_LOD, maxLod);
}
