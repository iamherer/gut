#include "gut/RenderBuffer.h"

void Gut::RenderBuffer::Create()
{
    glGenRenderbuffers(1, &ID);
}

Gut::RenderBuffer::~RenderBuffer()
{
    glDeleteRenderbuffers(1, &ID);
}

void Gut::RenderBuffer::Storage(unsigned int internalFormat, int width, int height)
{
    glBindRenderbuffer(GL_RENDERBUFFER, ID);
    glRenderbufferStorage(GL_RENDERBUFFER, internalFormat, width, height);
    glBindRenderbuffer(GL_RENDERBUFFER, 0);
}
void Gut::RenderBuffer::Bind()
{
    glBindRenderbuffer(GL_RENDERBUFFER, ID);
}