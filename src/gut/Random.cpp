#include "gut/Random.h"

void Gut::Random::GetInt(int *nos, int num, int min, int max)
{
    std::uniform_int_distribution<int> u(min, max);
    e.seed(time(0));

    for (int i = 0; i < num; i++)
    {
        nos[i] = u(e);
    }
}

void Gut::Random::GetFloat(float *fs, float num, float min, float max)
{
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<float> disX(min, max);

    for (int i = 0; i < num; ++i)
    {
        fs[i] = disX(gen);
    }
}

void Gut::Random::GetVec2(glm::vec2 *target, int num, const glm::vec2 &min, const glm::vec2 &max)
{
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<float> disX(min.x, max.x);
    std::uniform_real_distribution<float> disY(min.y, max.y);

    for (int i = 0; i < num; ++i)
    {
        target[i].x = disX(gen);
        target[i].y = disY(gen);
    }
}


void Gut::Random::GetVec3(glm::vec3* target , int num,const glm::vec3& min, const glm::vec3& max)
{
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<float> disX(min.x, max.x);
    std::uniform_real_distribution<float> disY(min.y, max.y);
    std::uniform_real_distribution<float> disZ(min.z, max.z);

    for (int i = 0; i < num; ++i)
    {
        target[i].x = disX(gen);
        target[i].y = disY(gen);
        target[i].z = disZ(gen);
    }
}

void Gut::Random::GetVec4(glm::vec4 *target, int num, const glm::vec4 &min, const glm::vec4 &max)
{
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<float> disX(min.x, max.x);
    std::uniform_real_distribution<float> disY(min.y, max.y);
    std::uniform_real_distribution<float> disZ(min.z, max.z);
    std::uniform_real_distribution<float> disW(min.w, max.w);

    for (int i = 0; i < num; ++i)
    {
        target[i].x = disX(gen);
        target[i].y = disY(gen);
        target[i].z = disZ(gen);
        target[i].w = disW(gen);
    }
}
