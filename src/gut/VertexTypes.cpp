#include "gut/VertexTypes.h"

using namespace Gut;

const std::vector<VertexLayout> VertexPos::inputLayout =
{
    {"POSITION", 0, 3, GL_FLOAT, GL_FALSE, offsetof(VertexPos, pos)},
};

const std::vector<VertexLayout> VertexPosNormal::inputLayout =
    {
        {"POSITION", 0, 3, GL_FLOAT, GL_FALSE, offsetof(VertexPosNormal, pos)},
        {"NORMAL", 1, 3, GL_FLOAT, GL_FALSE, offsetof(VertexPosNormal, normal)},
};

const std::vector<VertexLayout> VertexPosTex::inputLayout =
    {
        {"POSITION", 0, 3, GL_FLOAT, GL_FALSE, offsetof(VertexPosTex, pos)},
        {"TEXCOORD", 1, 2, GL_FLOAT, GL_FALSE, offsetof(VertexPosTex, tex)},
};

const std::vector<VertexLayout> VertexPosNormalTex::inputLayout =
{
    {"POSITION", 0, 3, GL_FLOAT, GL_FALSE, offsetof(VertexPosNormalTex, pos)},
    {"NORMAL", 1, 3, GL_FLOAT, GL_FALSE, offsetof(VertexPosNormalTex, normal)},
    {"TEXCOORD", 2, 2, GL_FLOAT, GL_FALSE, offsetof(VertexPosNormalTex, tex)},
};