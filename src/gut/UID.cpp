#include "gut/UID.h"

namespace {
	static std::random_device s_RandomDevice;
	static std::mt19937_64 s_Engine(s_RandomDevice());
	static std::uniform_int_distribution<uint64_t> s_UniformDistribution;
}

using namespace Gut;


UID::UID()
	:m_UUID(s_UniformDistribution(s_Engine))
{

};


