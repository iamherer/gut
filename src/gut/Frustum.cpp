#include "gut/Frustum.h"

using namespace Gut;

glm::mat4 Frustum::GetProjection() const
{
	return glm::perspective(glm::radians(fovy), aspect, nNear, nFar);
}

glm::mat4 Gut::Frustum::GetProjection(const float &aspect1) const
{
	return glm::perspective(glm::radians(fovy), aspect1, nNear, nFar);
}
