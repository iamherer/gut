#include "gut/Framebuffer.h"

void Gut::FrameBuffer::BufferTextureLayer(unsigned int attachment, unsigned int texId, int level, int layer)
{
	glBindFramebuffer(GL_FRAMEBUFFER, FBO);
	glFramebufferTextureLayer(GL_FRAMEBUFFER, attachment, texId, level, layer);
}

void Gut::FrameBuffer::BufferTexture2D(unsigned int attachment, unsigned int texTarget, unsigned int ID, unsigned int mipLevel)
{
	glBindFramebuffer(GL_FRAMEBUFFER, FBO);
	glFramebufferTexture2D(GL_FRAMEBUFFER, attachment, texTarget, ID, mipLevel);
}

void Gut::FrameBuffer::BufferTextureLayer(unsigned int attachment, unsigned int tex, unsigned int mipLevel, unsigned int zOff)
{
	glBindFramebuffer(GL_FRAMEBUFFER, FBO);
	glFramebufferTextureLayer(GL_FRAMEBUFFER, attachment, tex, mipLevel, zOff);
}

void Gut::FrameBuffer::FrameRenderBuffer(unsigned int attachment, unsigned int RBO)
{
	glBindFramebuffer(GL_FRAMEBUFFER, FBO);
	glBindRenderbuffer(GL_RENDERBUFFER, RBO);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, attachment, GL_RENDERBUFFER, RBO);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void Gut::FrameBuffer::ReadBuffer(unsigned int attachment)
{
	glBindFramebuffer(GL_FRAMEBUFFER, this->FBO);
	glReadBuffer(attachment);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void Gut::FrameBuffer::DrawBuffers(unsigned int* attachments, int num)
{
	glBindFramebuffer(GL_FRAMEBUFFER, FBO);
	glDrawBuffers(num, attachments);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void Gut::FrameBuffer::Create()
{
	glDeleteFramebuffers(1, &FBO);
	glGenFramebuffers(1, &FBO);
}

void Gut::FrameBuffer::CheckError()
{
	// - Finally check if framebuffer is complete
	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		GUT_LOGI("Framebuffer not complete!");
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void Gut::FrameBuffer::Bind()
{
	glBindFramebuffer(GL_FRAMEBUFFER, this->FBO);
}

void Gut::FrameBuffer::BufferTexture(unsigned int attachment, unsigned int texId, int level)
{
	glBindFramebuffer(GL_FRAMEBUFFER, FBO);
	// glFramebufferTexture(GL_FRAMEBUFFER, attachment, texId, level);
}

void Gut::FrameBuffer::BufferTexture3D(unsigned int attachment, unsigned int texTarget, unsigned int tex, unsigned int mipLevel, unsigned int zOff)
{
	glBindFramebuffer(GL_FRAMEBUFFER, FBO);
	// glFramebufferTexture3D(GL_FRAMEBUFFER, attachment, texTarget, tex, mipLevel, zOff);
}

Gut::FrameBuffer::~FrameBuffer()
{
	glDeleteFramebuffers(1, &FBO);
}

void Gut::FrameBuffer::UnBind()
{
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}